
public class gravitacija{
    
    public static double gravitacija(double visina){
        //M
        double konstanta1 = -11;
        double gravitacijskaKonstanta = 6.674 * Math.pow(10, konstanta1);
        //r
        double konstanta2 = 24;
        double masaZemlje = 5.972 * Math.pow(10, konstanta2);
        //C
        double konstanta3 = -11;
        double gravitacijskiPospesek = 6.674 * Math.pow(10, konstanta3);
        
        //R^2
        double r = 6.371 * Math.pow(10,6);
        
        double a = 6.674*Math.pow(10,-11)*5.972*Math.pow(10,24)/Math.pow(6.371e6 + visina,2);
        
        return a;
        
        
    }
    
    public static void main(String[] args){
        System.out.println("ois je zakon");
        double visina = Double.parseDouble(args[0]);
        izpis(visina,gravitacija(visina));
        
    }
    public static void izpis(double visina, double gravitacija){
        System.out.println(visina);
        System.out.println(gravitacija);
    }
}